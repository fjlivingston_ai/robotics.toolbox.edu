% ME555_06_Lecture7_Examples.m
% Fred.Livingston@duke.edu (09-05-2019)

clear
close all

%SE3
%tranimate
%tpoly

T0 = SE3(0,0,0);
T1 = transl(1,2,3)*rpy2tr(0.6,0.8,1.4);

figure;
trplot(T0, 'frame', 'T0', 'color', 'r')
figure;
trplot(T1, 'frame', 'T1', 'color', 'k')
figure;
tranimate(T0, T1)


T = [1 0 0 0; 0 1 0 0; 1 3 9 27; 0 1 6 27]
Tinv = inv(T)
q = [0 0 100 0]'
a = Tinv*q


tpoly(0,1,50)
