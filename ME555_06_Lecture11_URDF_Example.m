% ME555_06_Lecture11_Examples.m
% Fred.Livingston@duke.edu (09.26.2019)


clear;
clc;
close all;


% help Link
% help SerialLink

%RR Manipulator
a0 = 1;
a1 = 1;
a2 = 1;

% 2 Link D-H Table
L(1)       = Link('alpha', 0, 'a',  0, 'd', a0);
L(2)       = Link('alpha', 0, 'a', a1, 'd', 0);
L(3)       = Link('alpha', 0, 'a', a2, 'd', 0);


% Create Robot Manipulators
R1          = SerialLink(L, 'name','Livingston URDF Manipulator')
R1.plotopt  = {'workspace' [-2,3,-2,3,-1,3]};
R1.plot([0 0 0]);
R1.teach

clear
syms a0 a1 a2 q1 q2
L(1)       = Link('alpha', 0, 'a',  0, 'd', a0);
L(2)       = Link('alpha', 0, 'a', a1, 'd', 0);
L(3)       = Link('alpha', 0, 'a', a2, 'd', 0);
R2          = SerialLink(L, 'name','Livingston RR Manipulator')
R2.fkine([0 q1 q2])

clear
% SCARA Example
d0 = 0.5
a1 = 0.7
a2 = 0.7
L(1)       = Link('alpha', 0, 'a',  0, 'd', d0);
L(2)       = Link('alpha', 0, 'a', a1, 'd', 0);
L(3)       = Link('alpha', 0, 'a', a2, 'd', 0);
L(4)       = Link('alpha', 0, 'a', 0, 'theta', 0);
L(4).qlim  = [0.1 3];
L(5)       = Link('alpha', 0, 'a', 0,  'd', 0);
R3          = SerialLink(L, 'name','Livingston RR Manipulator')
R3.plotopt  = {'workspace' [-2,3,-2,3,-1,3]};
R3.teach

