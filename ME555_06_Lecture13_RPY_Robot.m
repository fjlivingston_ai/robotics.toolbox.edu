%RRP_Robot.m
clear;
clc;
close all;

%RPY Manipulator
d3 = 1;

% 3 Link D-H Table
L(1)        = Link('alpha', -pi/2 , 'a', 0, 'd', 0);
L(2)        = Link('alpha', -pi/2 , 'a', 0, 'd', 0);
L(3)        = Link('alpha', 0     , 'a', 0, 'd', d3);
L(2).offset = -pi/2;

% Create Robot Manipulators
R1          = SerialLink(L, 'name','Livingston RPY Manipulator')
R1.plotopt  = {'workspace' [-2,3,-2,3,-1,3]};
R1.teach


