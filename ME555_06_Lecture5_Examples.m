% ME555_06_Lecture5_Examples.m
% Fred.Livingston@duke.edu (09-05-2019)

clear
close all

%oa2r
R = eul2r(0.1,0.2,0.3)
R = r2t(R)
T = SE3(R)
o = T.o
a = T.a

n = cross(o,a)


figure
trplot(R)
eig(R)  
tr2angvec(R)

figure
q = UnitQuaternion(R)
q.plot
q * inv(q)

s = sqrt(2)/2
v1 = 0
v2 = 0
v3 = sqrt(2)/2
q = Quaternion([s,v1,v2,v3])
q = UnitQuaternion(q)
q.R
figure
q.plot
