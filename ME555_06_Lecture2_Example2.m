% ME555_06_Lecture2_Example2.m
% Fred.Livingston@duke.edu (08-28-2019)

clear
close all

T1 = transl2(1,2)
R1 = trot2(30, 'deg')

T2 = T1*R1
T2 = SE2(1, 2, 30, 'deg')

figure
trplot2(T2, 'frame', '2', 'color', 'b')
grid on

T3 = SE2(2,1,0)
hold on
trplot2(T3, 'frame', '3', 'color', 'r')
axis([0 5 0 5])

T4 = T2*T2
trplot2(T4, 'frame' ,'4', 'color' ,'g')