% ME555_06_Lecture10_Examples.m
% Fred.Livingston@duke.edu (09.26.2019)


clear;
clc;
close all;


% help Link
% help SerialLink

%RR Manipulator
a1 = 1;
a2 = 1;

% 2 Link D-H Table
L(1)       = Link('alpha', 0, 'a', a1, 'd', 0);
L(2)       = Link('alpha', 0, 'a', a2, 'd', 0);


% Create Robot Manipulators
R1          = SerialLink(L, 'name','Livingston RR Manipulator')
R1.plotopt  = {'workspace' [-2,3,-2,3,-1,3]};
R1.plot([0 0]);
R1.teach

clear
syms a1 a2 q1 q2
L(1)       = Link('alpha', 0, 'a', a1, 'd', 0);
L(2)       = Link('alpha', 0, 'a', a2, 'd', 0);
R1          = SerialLink(L, 'name','Livingston RR Manipulator')
R1.fkine([q1 q2])

