% ME555_06_Lecture6_Examples.m
% Fred.Livingston@duke.edu (09-05-2019)

clear
close all

%oa2r
%SO3
R = rotz(45,'deg')
R = SO3(R)
n = R.n
a = R.a
o = R.o
n1 = cross(o,a)

clear
R = rotz(45,'deg')
tr2angvec(R,'deg')

q = UnitQuaternion(R)
q.R

