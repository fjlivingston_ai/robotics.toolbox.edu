% ME555_06_Lecture7_Examples.m
% Fred.Livingston@duke.edu (09-05-2019)

clear
close all

%lspb
%tranimate
%tpoly


lspb(0,1,50)
T = [1 0 0 0; 0 1 0 0; 1 10 100 1000; 0 1 20 300]
Q = [0 0 1 0]'
inv(T)
a = inv(T)*Q
A = a(1)
B = a(2)
C = a(3)
D = a(4)

tf = 10
t = [0:1:tf]
s = A + B*t + C*power(t,2) + D*power(t,3)

Xo =10;
Xf =30;
Yo =20;
Yf =10;
X = (1-s)*Xo + s*Xf;
Y = (1-s)*Yo + s*Yf;

figure;
plot(t,s)
grid on
xlabel('Time [seconds]');
ylabel('S');

figure;
plot(t,X);
hold on
plot(t,Y);
grid on;
xlabel('Time [seconds]');
ylabel('Position');
legend('X', 'Y');

T0 = SE3(0,0,0)
T1 = SE3(trotz(45,'deg'))
q0 = T0.UnitQuaternion
q1 = T1.UnitQuaternion

figure
trplot(T0,'frame', 'T0', 'color', 'r')
figure
trplot(T1,'frame', 'T1', 'color', 'k')
q0.interp(q1,0.5)


