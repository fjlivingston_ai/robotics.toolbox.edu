% ME555_06_Lecture2_Example3.m
% Fred.Livingston@duke.edu (08-28-2019)

clear
close all

P = [0 2 0]'
R1 = rotz(30, 'deg')
P2 = R1*P

plot3(P(1), P(2), P(3), '*')
grid on
hold on
plot3(P2(1), P2(2), P2(3), '*')

clear
P = [4 3 2]'
R1 = rotx(-30, 'deg')
P2 = R1*P

