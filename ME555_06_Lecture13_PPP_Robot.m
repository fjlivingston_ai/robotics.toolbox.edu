%PPP_Robot.m
clear;
clc;
close all;

%RRP Manipulator
L1 = 1;

% 3 Link D-H Table
L(1)       = Link('alpha', -pi/2 , 'a', 0, 'theta', 0);
L(2)       = Link('alpha', -pi/2 , 'a', 0, 'theta', -pi/2);
L(3)       = Link('alpha', 0     , 'a', 0, 'theta', 0);
L(1).qlim  = [0.1 2];
L(2).qlim  = [0.1 2];
L(3).qlim  = [0.1 2];

% Create Robot Manipulators
R1          = SerialLink(L, 'name','Livingston PPP Manipulator')
R1.plotopt  = {'workspace' [-2,3,-2,3,-1,3]};
R1.plot([1 1 1]);
R1.teach


