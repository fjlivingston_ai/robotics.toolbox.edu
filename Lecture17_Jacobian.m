%Lecture17_Jacobian.m
clear;
clc;
close all;

%Robot Manipulator
L1 = 1;
L2 = 1;
L3 = 1;

% 3 Link D-H Table
L(1)        = Link('alpha', 0  , 'a', L1,  'd', 0);
L(2)        = Link('alpha', 0  , 'a', L2,  'd', 0);
L(3)        = Link('alpha', 0  , 'a', L2,  'd', 0);

% Create Robot Manipulators
R1          = SerialLink(L, 'name','Livingston 3R Manipulator')
R1.plotopt  = {'workspace' [-2,3,-2,3,-1,4]};

%tpoly(1.707,1.5,10)

Xg = 1.5;  % X Goal
Yg = 1.5;  % Y Goal
Wg = 0;    % Orientation Goal

% Initial Configuration
qk1=0;   
qk2=45;
qk3=45;

doLoop = true;
while doLoop
    
%Compute FK
FK = R1.fkine([qk1 qk2 qk3], 'deg');
xk = FK.t(1)  % compute x location
yk = FK.t(2)  % compute y location

J = R1.jacob0([qk1 qk2 qk3], 'deg');
J_inv = pinv(J);
X = Xg - xk;
Y = Yg - yk;
Xv = [X Y 0 0 0 Wg]';
Q = J_inv*Xv;

% Adjust Joint Vars
qk1 = qk1 + Q(1);
qk2 = qk2 + Q(2);
qk3 = qk3 + Q(3);

R1.plot([qk1 qk2 qk3], 'deg');

if xk == Xg  % <- Your control to break the loop
     doLoop = false;
  end
end

