% ME555_06_Lecture9_Examples.m
% Fred.Livingston@duke.edu (09-24-2019)

clear
close all

%trchain2
%syms
%mdl_planer1

a1 = 1;    % unit length
q1 = 0.2;  % rads

T = trchain2('R(q1) Tx(a1)', q1)

syms q1 a1
trchain2('R(q1) Tx(a1)', q1)

mdl_planar1
%p1.plot([0])
p1.teach([0])

clear
syms q1 q2 a1 a2
T = trchain2('R(q1) Tx(a1) R(q2) Tx(a2)', [q1 q2])
mdl_planar2
p2.teach