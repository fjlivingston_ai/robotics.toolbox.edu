% ME555_06_Lecture3_Examples.m
% Fred.Livingston@duke.edu (09-02-2019)

clear
close all

P = [0 2 0]'
R1 = rotz(30, 'deg')
P2 = R1*P

plot3(P(1), P(2), P(3), '*')
grid on
hold on
plot3(P2(1), P2(2), P2(3), '*')

clear
P = [4 3 2]'
R1 = rotx(-30, 'deg')
P2 = R1*P

clear 
R1 = rotx(90, 'deg')*roty(90, 'deg')
R2 = roty(90, 'deg')*rotx(90, 'deg')
figure;
trplot(R1, 'frame', 'R1' ,'color', 'b')
hold on
trplot(R2, 'frame', 'R2' , 'color' , 'r')


