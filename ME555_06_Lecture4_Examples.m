% ME555_06_Lecture4_Examples.m
% Fred.Livingston@duke.edu (09-05-2019)

clear
close all

T1 = [0.5 -0.1464 0.8536 1; 0.5 0.8536 -0.1464 0; -0.7071 0.5 0.5 0; 0 0 0 1]
inv(T1)

trplot(T1, 'frame','T1')


