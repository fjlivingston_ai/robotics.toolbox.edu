%RR_Robot.m
clear;
clc;
close all;

%RR Manipulator
a1 = 1;
a2 = 1;


% 2 Link D-H Table
L(1)        = Link('alpha', 0    , 'a', a1, 'd', 0);
L(2)        = Link('alpha', 0    , 'a', a2, 'd', 0);

% Create Robot Manipulators
R1          = SerialLink(L, 'name','Livingston RR Manipulator')
R1.plotopt  = {'workspace' [-2,3,-2,3,-1,3] 'top'};
R1.teach('callback', @vellipse)


