% ME555_06_Lecture2_Example1.m
% Fred.Livingston@duke.edu (08-28-2019)
clear
close all

P1 = [3; 4]
P1 = [3 4]'
R1 = rot2(-60, 'deg')
P2 = R1*P1
det(R1)
R2 = inv(R1)
R2 = R1'
P1 = R2*P2

trplot2(R1, 'frame', '1', 'color', 'k')
axis 'equal'
hold on
trplot2(R2, 'frame', '2', 'color', 'b')
grid on