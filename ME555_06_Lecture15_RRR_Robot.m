%RRR_Robot.m
clear;
clc;
close all;

%RRR Manipulator
a1 = 1;
a2 = 1;
a3 = 1;

% 3 Link D-H Table
L(1)        = Link('alpha', 0, 'a', a1, 'd', 0);
L(2)        = Link('alpha', 0    , 'a', a2, 'd', 0);
L(3)        = Link('alpha', 0    , 'a', a3, 'd', 0);

% Create Robot Manipulators
R1          = SerialLink(L, 'name','Livingston RRR Manipulator')

T0  = R1.fkine([0, 45, 45], 'deg')
Td = R1.fkine([1, 45, 45], 'deg')

T = (T0 - Td)/1




